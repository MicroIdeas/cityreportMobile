package com.innova.citylink.helpers.comunication;

public interface IFragmentCommunicator {
	public void passDataToFragment(String someValue);
}
