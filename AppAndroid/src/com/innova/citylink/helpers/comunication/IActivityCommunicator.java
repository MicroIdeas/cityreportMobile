package com.innova.citylink.helpers.comunication;

import org.apache.http.entity.mime.content.ByteArrayBody;

import com.google.android.gms.maps.model.LatLng;

public interface IActivityCommunicator {

	public void setStreetLocation(String streetLocationValue);

	public void setLatLng(LatLng latLngValue);

	public void setPhotoReport(ByteArrayBody byteArrayBodyValue);

}
