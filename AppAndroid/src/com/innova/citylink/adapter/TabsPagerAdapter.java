package com.innova.citylink.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.innova.citylink.feedreader.ArticleListFragment;
import com.innova.citylink.fragments.CategoryListReportFragment;
import com.innova.citylink.fragments.HomeFragment;

/**
 * @Description: This adapter provides fragment views to tabs
 * 
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
	
	private String mToken;
	
	public TabsPagerAdapter(FragmentManager fm, String mToken) {
		super(fm);
		this.mToken = mToken;;
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Home fragment activity
			return new HomeFragment();
		case 1:
			// Reports fragment activity
			Bundle args = new Bundle();
		    args.putString("token", mToken);
			
			CategoryListReportFragment categoryListReportFragment = new CategoryListReportFragment();
			categoryListReportFragment.setArguments(args);
			return categoryListReportFragment;
		case 2:
			// News fragment activity
			return new ArticleListFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}

}