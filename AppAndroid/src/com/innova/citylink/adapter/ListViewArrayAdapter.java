package com.innova.citylink.adapter;

import com.innova.citylink.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListViewArrayAdapter extends ArrayAdapter<String> {

	public ListViewArrayAdapter(Context context, int resource,
			int textViewResourceId, String[] objects) {
		super(context, resource, textViewResourceId, objects);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// 1. Create inflater
		LayoutInflater inflater = (LayoutInflater) super.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		// 2. Get rowView from inflater
		View rowView = null;
		if (!super.getItem(position).startsWith("GROUP:")) {
			rowView = inflater.inflate(R.layout.list_item, parent, false);

			// 3. Get title views from the rowView
			TextView titleView = (TextView) rowView.findViewById(R.id.label);

			// 4. Set the text for textView
			titleView.setText(super.getItem(position));
		} else {
			rowView = inflater.inflate(R.layout.group_header_item, parent,
					false);
			TextView titleView = (TextView) rowView.findViewById(R.id.header);
			titleView.setText(super.getItem(position).substring(6));

			rowView.setOnClickListener(null);
		}

		// 5. retrn rowView
		return rowView;
	}
}
