package com.innova.citylink.sliding;

import android.support.v4.app.Fragment;

import com.innova.citylink.feedreader.ArticleListFragment;
import com.innova.citylink.fragments.CategoryListReportFragment;
import com.innova.citylink.fragments.HomeFragment;
import com.innova.citylink.fragments.PerfilFragment;

public class SamplePagerItem {

	private final CharSequence mTitle;
	private final int mIcon;
	private final int mIndicatorColor;
	private final int mDividerColor;
	private final int position;

	private Fragment[] listFragments;

	public SamplePagerItem(int position, CharSequence title, int mIcon,
			int indicatorColor, int dividerColor, String mToken) {
		this.mTitle = title;
		this.mIcon = mIcon;
		this.position = position;
		this.mIndicatorColor = indicatorColor;
		this.mDividerColor = dividerColor;

		listFragments = new Fragment[] { HomeFragment.newInstance(),
				CategoryListReportFragment.newInstance(mToken),
				ArticleListFragment.newInstance(), PerfilFragment.newInstance(mToken) };
	}

	public Fragment createFragment() {
		return listFragments[position];
	}

	public CharSequence getTitle() {
		return mTitle;
	}

	public int getIcon() {
		return mIcon;
	}

	public int getIndicatorColor() {
		return mIndicatorColor;
	}

	public int getDividerColor() {
		return mDividerColor;
	}
}
