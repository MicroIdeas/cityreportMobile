package com.innova.citylink.login;

public class LoginConstant {
	public static String URL_REGISTER = "http://citylink.leadprogrammer.com.ar/cityReportService/rest/user/";
	public static String URL_SIGN_IN = "http://citylink.leadprogrammer.com.ar/cityReportService/rest/user/login/";
	public static String URL_CATEGORY = "http://citylink.leadprogrammer.com.ar/cityReportService/rest/category";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE_VALUE = "application/json";
	public final static int GET = 1;
	public final static int POST = 2;
	
	public static final String CATEGORIES_JSON = "categories.json";

	public static final String PASSWORD = "password";
	public static final String USER = "user";
	public static final String EMAIL_ADDRESS = "emailAddress";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String USERNAME = "username";
	
}
