package com.innova.citylink.login;

import com.innova.citylink.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class LoginActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login);

		Button btnLogin = (Button) findViewById(R.id.btnLogin);
		Button btnRegisterEmail = (Button) findViewById(R.id.btnRegisterEmail);
		
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				Intent loginScreen = new Intent(getApplicationContext(), SignInActivity.class);
				
				startActivity(loginScreen);
			}
		});
		
		btnRegisterEmail.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View arg0) {
				Intent loginScreen = new Intent(getApplicationContext(), RegisterActivity.class);
				
				startActivity(loginScreen);
			}
		});
	}
}
