package com.innova.citylink.feedreader.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

	public static String getDateDifference(Date thenDate) {
		Calendar now = Calendar.getInstance();
		Calendar then = Calendar.getInstance();
		now.setTime(new Date());
		then.setTime(thenDate);

		// Get the represented date in milliseconds
		long nowMs = now.getTimeInMillis();
		long thenMs = then.getTimeInMillis();

		// Calculate difference in milliseconds
		long diff = nowMs - thenMs;

		// Calculate difference in seconds
		long diffMinutes = diff / (60 * 1000);
		long diffHours = diff / (60 * 60 * 1000);
		long diffDays = diff / (24 * 60 * 60 * 1000);

		if (diffMinutes < 60) {
			if (diffMinutes == 1)
				return diffMinutes + " hace un minuto";// minute ago
			else
				return diffMinutes + " hace minutos";// minutes ago
		} else if (diffHours < 24) {
			if (diffHours == 1)
				return diffHours + " hace una hora";// hour ago
			else
				return diffHours + " hace horas";// hours ago
		} else if (diffDays < 30) {
			if (diffDays == 1)
				return diffDays + " hace un d�a";// day ago
			else
				return diffDays + " hace d�as";// days ago
		} else {
			return "hace mucho tiempo..";// a long time ago
		}
	}

}
