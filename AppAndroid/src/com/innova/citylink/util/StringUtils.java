package com.innova.citylink.util;

public class StringUtils {
	
	public static boolean isEmpty(String input) {
		if (input != null && input.length() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isNotEmpty(String input) {
		if (input != null && input.length() > 0) {
			return true;
		}
		return false;
	}
}
