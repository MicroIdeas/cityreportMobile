package com.innova.citylink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.innova.citylink.adapter.TabsPagerAdapter;
import com.innova.citylink.adapter.ViewPagerAdapter;
import com.innova.citylink.login.LoginActivity;
import com.innova.citylink.login.sessions.UserSessionManager;
import com.innova.citylink.sliding.SamplePagerItem;
import com.innova.citylink.sliding.SlidingTabLayout;
import com.innova.citylink.util.Utils;

public class MainActivity extends FragmentActivity {

	private List<SamplePagerItem> mTabs = new ArrayList<SamplePagerItem>();
	private static Theme theme = null;
//	private ViewPager viewPager;
//	private TabsPagerAdapter mAdapter;
//	private ActionBar actionBar;
	private String mToken;
	private ViewPager mPager;
	
	// Tab titles //TODO pasar a properties
//	private String[] tabs = { "Inicio", "Reporar", "Noticias" };

	// User Session Manager Class
	UserSessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_main);
		setContentView(R.layout.viewpager_fragment);

		// User Session Manager
		session = new UserSessionManager(getApplicationContext());

		// Check user login
		// If User is not logged in , This will redirect user to LoginActivity.
		if (session.checkLogin()) {
			Intent i = new Intent(getApplicationContext(), LoginActivity.class);
			// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// Add new Flag to start new Activity
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(i);
		}
		
		Intent intent = getIntent();
		mToken = intent.getStringExtra("token");
		
		// get user data from session
		HashMap<String, String> userSession = session.getUserDetails();

		// get name
		String name = userSession.get(UserSessionManager.KEY_NAME);

		// get email
		String email = userSession.get(UserSessionManager.KEY_EMAIL);
		
		if (mToken == null) {
			mToken = userSession.get(UserSessionManager.KEY_TOKEN);
		}
		
		theme = getTheme();

		// Se debe agregar el fragment correspondiente al orden de la tab en
		// SamplePagerItem.java. Mejorar este codigo.
		mTabs.add(new SamplePagerItem(0, "",
				getThemeAttr(R.attr.icon_home), getResources().getColor(
						Utils.colors[0]), Color.TRANSPARENT, mToken));
		mTabs.add(new SamplePagerItem(1, "",
				getThemeAttr(R.attr.icon_report), getResources().getColor(
						Utils.colors[2]), Color.TRANSPARENT, mToken));
		mTabs.add(new SamplePagerItem(2, "",
				getThemeAttr(R.attr.icon_rss), getResources().getColor(
						Utils.colors[4]), Color.TRANSPARENT, mToken));
		mTabs.add(new SamplePagerItem(3, "",
				getThemeAttr(R.attr.icon_perfil), getResources().getColor(
						Utils.colors[6]), Color.TRANSPARENT, mToken));

		// Initilization
//		viewPager = (ViewPager) findViewById(R.id.pager);
//		actionBar = getActionBar();
//		mAdapter = new TabsPagerAdapter(getSupportFragmentManager(), mToken);

		// The following two options trigger the collapsing of the main action
		// bar view.
		// See the parent activity for the rest of the implementation
//		actionBar.setDisplayShowHomeEnabled(false);
//		actionBar.setDisplayShowTitleEnabled(false);

//		viewPager.setAdapter(mAdapter);
		// actionBar.setHomeButtonEnabled(false);
//		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
//		for (String tab_name : tabs) {
//			actionBar.addTab(actionBar.newTab().setText(tab_name)
//					.setTabListener(this));
//		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
//		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//			@Override
//			public void onPageSelected(int position) {
//				// on changing the page
//				// make respected tab selected
////				actionBar.setSelectedNavigationItem(position);
//			}
//
//			@Override
//			public void onPageScrolled(int arg0, float arg1, int arg2) {
//			}
//
//			@Override
//			public void onPageScrollStateChanged(int arg0) {
//			}
//		});
		findViewById(R.id.mTabs);
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.mPager);
        
        leo();
	}
	
	public static int getThemeAttr(int attr) {
		TypedValue typedvalueattr = new TypedValue();
		theme.resolveAttribute(attr, typedvalueattr, true);

		return typedvalueattr.resourceId;
	}
	
	
	public void leo() {
		
		
		
		ViewPager mViewPager = mPager;
//		ViewPager mViewPager = (ViewPager) mPager.findViewById(R.id.mPager);

		mViewPager.setOffscreenPageLimit(3);
		mViewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager(),
				mTabs));

//		SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) mViewPager
//				.findViewById(R.id.mTabs);
		SlidingTabLayout mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.mTabs);
		mSlidingTabLayout.setBackgroundResource(R.color.white);
		mSlidingTabLayout.setViewPager(mViewPager);

		mSlidingTabLayout
				.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

					@Override
					public int getIndicatorColor(int position) {
						return mTabs.get(position).getIndicatorColor();
					}

					@Override
					public int getDividerColor(int position) {
						return mTabs.get(position).getDividerColor();
					}
				});
	}
	
	
//	@Override
//	public void onTabReselected(Tab tab, FragmentTransaction ft) {
//	}
//
//	@Override
//	public void onTabSelected(Tab tab, FragmentTransaction ft) {
//		// on tab selected
//		// show respected fragment view
//		viewPager.setCurrentItem(tab.getPosition());
//	}
//
//	@Override
//	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
//	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// if (id == R.id.action_settings) {
	// return true;
	// }
	// return super.onOptionsItemSelected(item);
	// }

	/**
	 * Event Handling for Individual menu item selected Identify single menu
	 * item by it's id
	 * */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.menu_acerca_de:
			Intent i = new Intent(getApplicationContext(), AboutUsActivity.class);
			startActivity(i);
			
			return true;
		case R.id.menu_close_session:
			// Toast.makeText(this, "Preferences is Selected",
			// Toast.LENGTH_SHORT)
			// .show();
			session.logoutUser();

			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
