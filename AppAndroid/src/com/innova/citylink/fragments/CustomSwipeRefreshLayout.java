package com.innova.citylink.fragments;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.widget.ListView;
import android.widget.ScrollView;

public class CustomSwipeRefreshLayout extends SwipeRefreshLayout {
	private ListView scrollview;

	public CustomSwipeRefreshLayout(Context context) {
		super(context);
	}

	public CustomSwipeRefreshLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setView(ListView view) {
		this.scrollview = view;
	}

	@Override
	public boolean canChildScrollUp() {
		return scrollview.getFirstVisiblePosition() != 0;
	}
}
