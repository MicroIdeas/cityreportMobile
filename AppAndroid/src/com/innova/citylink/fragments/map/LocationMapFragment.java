package com.innova.citylink.fragments.map;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.innova.citylink.R;
import com.innova.citylink.photo.PhotoUtil;
import com.innova.citylink.util.GeocodeJSONParser;

public class LocationMapFragment extends FragmentActivity {

	/**
	 * Note that this may be null if the Google Play services APK is not
	 * available.
	 */
	private GoogleMap mMap;
	private Button mBtnFind;
	private EditText etPlace;
	private String locationStreet = null;
	private LatLng locationLatLng = null;
	Marker marker = null;
	GroundOverlay groundOverlay2 = null;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_map_fragment);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		// Getting reference to the find button
		mBtnFind = (Button) findViewById(R.id.btn_show);

		// Getting reference to EditText
		etPlace = (EditText) findViewById(R.id.et_place);

		setUpMapIfNeeded();
		updateMyLocation();

		mBtnFind.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Getting the place entered
				getLocation();
			}
		});
	}

	private void getLocation() {
		String location = etPlace.getText().toString();

		location = normalizeLocation(location);

		if (location == null || location.equals("")) {
			Toast.makeText(getBaseContext(), "No Place is entered",
					Toast.LENGTH_SHORT).show();
			return;
		}

		String url = "https://maps.googleapis.com/maps/api/geocode/json?";

		try {
			// encoding special characters like space in the user input place
			location = URLEncoder.encode(location, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		String address = "address=" + location;

		String sensor = "sensor=false";

		// url , from where the geocoding data is fetched
		url = url + address + "&" + sensor;

		// String modifiedURL= url.toString().replace(" ", "%20");

		// Instantiating DownloadTask to get places from Google Geocoding
		// service
		// in a non-ui thread
		DownloadTask downloadTask = new DownloadTask();

		// Start downloading the geocoding places
		downloadTask.execute(url);
	}

	private static String CITY = "Cordoba";
	private static String STATE = "Cordoba";
	private static String COUNTRY = "Argentina";

	private String normalizeLocation(String location) {

		location = replaceAccent(location);
		location.replace(CITY, "");
		location.replace(STATE, "");
		location.replace(COUNTRY, "");
		location.replace(",", "");
		location = location.trim();
		location = location + ", C�rdoba, C�rdoba, Argentina";

		// Format: Avenida Col�n 1000, C�rdoba, Cordoba, Argentina
		return location;
	}

	/**
	 * Funci�n que elimina acentos y caracteres especiales de una cadena de
	 * texto.
	 * 
	 * @param input
	 * @return cadena de texto limpia de acentos y caracteres especiales.
	 */
	public static String replaceAccent(String input) {
		// Cadena de caracteres original a sustituir.
		String original = "��������������u�������������������";
		// Cadena de caracteres ASCII que reemplazar�n los originales.
		String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
		String output = input;
		for (int i = 0; i < original.length(); i++) {
			// Reemplazamos los caracteres especiales.
			output = output.replace(original.charAt(i), ascii.charAt(i));
		}// for i
		return output;
	}

	private void updateMyLocation() {
		if (!checkReady()) {
			return;
		}
		mMap.setMyLocationEnabled(true);

		// ///// ZOOM CAMERA:

		// Getting LocationManager object from System Service LOCATION_SERVICE
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		// Creating a criteria object to retrieve provider
		Criteria criteria = new Criteria();

		// Getting the name of the best provider
		String provider = locationManager.getBestProvider(criteria, true);

		// Getting Current Location
		Location location = locationManager.getLastKnownLocation(provider);

		if (location != null) {
			onLocationChanged(location);
		}

	}

	private boolean checkReady() {
		if (mMap == null) {
			Toast.makeText(this, "Mapa no listo", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	// @Override
	public void onLocationChanged(Location location) {

		// Getting latitude of the current location
		double latitude = location.getLatitude();

		// Getting longitude of the current location
		double longitude = location.getLongitude();

		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(latitude, longitude);

		// Showing the current location in Google Map
		mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
		}

		return super.onOptionsItemSelected(item);
	}

	public void btnAcceptLocation(View view) {

		if (view == findViewById(R.id.btnAcceptLocation)) {
			// String resultado = etPlace.getText().toString();
			File file = captureMapScreen();

			if (locationStreet == null || locationStreet == ""
					|| !locationLatLng.equals(mMap.getCameraPosition().target)) {
				locationLatLng = mMap.getCameraPosition().target;
				locationStreet = getCompleteAddressString(locationLatLng);
			}

			Intent i = getIntent();
			i.putExtra("LOC_STREET", locationStreet);
			i.putExtra("LOC_LATLNG", locationLatLng);
			i.putExtra("snapshotMap", file.getAbsolutePath());

			setResult(1001, i);

			finish();
		}
	}

	public void btnCancelLocation(View view) {
		finish();
	}

	private String getCompleteAddressString(LatLng locationLatLng) {
		String addressText = "";
		Geocoder geocoder = new Geocoder(this);

		try {
			List<Address> addresses = geocoder.getFromLocation(
					locationLatLng.latitude, locationLatLng.longitude, 1);

			if (addresses != null && !addresses.isEmpty()) {
				Address address = addresses.get(0);

				addressText = String.format(
						"%s, %s, %s, %s",
						address.getMaxAddressLineIndex() > 0 ? address
								.getAddressLine(0) : "", address.getLocality(),
						address.getAdminArea(), address.getCountryName());
			} else {
				addressText = "Leo";
			}

			if (addressText.length() < 1) {
				addressText = "Leo2";
			}

		} catch (IOException e) {
			addressText = e.getMessage();
		}

		return addressText;
	}

	// ver si return file o path ?
	public File captureMapScreen() {
		final File file = PhotoUtil.getFile(this);

		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
			Bitmap bitmap;

			@Override
			public void onSnapshotReady(Bitmap snapshot) {
				// TODO Auto-generated method stub
				// bitmap = snapshot;
				int width = snapshot.getWidth();
				int height = snapshot.getHeight() / 3;

				bitmap = Bitmap
						.createBitmap(snapshot, 0, height, width, height);

				try {
					FileOutputStream ostream = new FileOutputStream(file);

					bitmap.compress(Bitmap.CompressFormat.PNG, 90, ostream);
					// TODO cortar imagen y reducir su escala.

					// FileOutputStream out = new
					// FileOutputStream("/mnt/sdcard/"
					// + "MyMapScreen" + System.currentTimeMillis()
					// + ".png");
					//
					// // above "/mnt ..... png" => is a storage path (where
					// image will be stored) + name of image you can customize
					// as per your Requirement
					//
					// bitmap.compress(Bitmap.CompressFormat.PNG, 50, out);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		// mMap.snapshot(callback);
		Bitmap bitmap2 = null;
		mMap.snapshot(callback, bitmap2);

		// galleryAddPic(file.getAbsolutePath());
		System.out.println("");
		// myMap is object of GoogleMap +> GoogleMap myMap;
		// which is initialized in onCreate() =>
		// myMap = ((SupportMapFragment)
		// getSupportFragmentManager().findFragmentById(R.id.map_pass_home_call)).getMap();

		return file;
	}

	// private void galleryAddPic(String mCurrentPhotoPath) {
	// Intent mediaScanIntent = new
	// Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
	// File f = new File(mCurrentPhotoPath);
	// Uri contentUri = Uri.fromFile(f);
	// mediaScanIntent.setData(contentUri);
	// this.sendBroadcast(mediaScanIntent);
	// }

	@Override
	protected void onResume() {
		super.onResume();
		setUpMapIfNeeded();
	}

	/**
	 * Sets up the map if it is possible to do so (i.e., the Google Play
	 * services APK is correctly installed) and the map has not already been
	 * instantiated.. This will ensure that we only ever call
	 * {@link #setUpMap()} once when {@link #mMap} is not null.
	 * <p>
	 * If it isn't installed {@link SupportMapFragment} (and
	 * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt
	 * for the user to install/update the Google Play services APK on their
	 * device.
	 * <p>
	 * A user can return to this FragmentActivity after following the prompt and
	 * correctly installing/updating/enabling the Google Play services. Since
	 * the FragmentActivity may not have been completely destroyed during this
	 * process (it is likely that it would only be stopped or paused),
	 * {@link #onCreate(Bundle)} may not be called again so we should call this
	 * method in {@link #onResume()} to guarantee that it will be called.
	 */
	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (mMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			MarkerOptions a = new MarkerOptions();
			a.position(mMap.getCameraPosition().target);
			//
			a.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
			marker = mMap.addMarker(a);

			// // add overlay
			// BitmapDescriptor image = BitmapDescriptorFactory
			// .fromResource(R.drawable.ic_marker);
			// GroundOverlayOptions groundOverlay = new GroundOverlayOptions()
			// .image(image)
			// .position(mMap.getCameraPosition().target, 60f);
			// // .transparency(0.5f);
			// groundOverlay2 = mMap.addGroundOverlay(groundOverlay);

			// Check if we were successful in obtaining the map.
			if (mMap != null) {
				setUpMap();
			}
		}
	}

	/**
	 * This is where we can add markers or lines, add listeners or move the
	 * camera.
	 * <p>
	 * This should only be called once and when we are sure that {@link #mMap}
	 * is not null.
	 */
	private void setUpMap() {
		// mMap.addMarker(new MarkerOptions().position(new LatLng(0,
		// 0)).title("Marker"));
		mMap.setOnCameraChangeListener(new OnCameraChangeListener() {
			public void onCameraChange(CameraPosition arg0) {
				// mMap.clear();
				marker.setPosition(mMap.getCameraPosition().target);
				// groundOverlay2.setPosition(mMap.getCameraPosition().target);
			}
		});
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		marker.setPosition(mMap.getCameraPosition().target);
		// groundOverlay2.setPosition(mMap.getCameraPosition().target);

		// Toast.makeText(this, "CameraPosition: " +
		// mMap.getCameraPosition().target,
		// Toast.LENGTH_LONG/1000).show();

		return super.dispatchTouchEvent(ev);
	}

	// ////////////////////////////////////////

	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();

			br.close();

		} catch (Exception e) {
			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}

		return data;

	}

	/** A class, to download Places from Geocoding webservice */
	private class DownloadTask extends AsyncTask<String, Integer, String> {

		String data = null;

		// Invoked by execute() method of this object
		@Override
		protected String doInBackground(String... url) {
			try {
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(String result) {

			ParserTask parserTask = new ParserTask();
			parserTask.execute(result);
		}

	}

	class ParserTask extends
			AsyncTask<String, Integer, List<HashMap<String, String>>> {

		JSONObject jObject;

		@Override
		protected List<HashMap<String, String>> doInBackground(
				String... jsonData) {

			List<HashMap<String, String>> places = null;
			GeocodeJSONParser parser = new GeocodeJSONParser();

			try {
				jObject = new JSONObject(jsonData[0]);

				/** Getting the parsed data as a an ArrayList */
				places = parser.parse(jObject);

			} catch (Exception e) {
				Log.d("Exception", e.toString());
			}
			return places;
		}

		// TODO: Guardar solo la ultima posicion.
		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(List<HashMap<String, String>> list) {

			// Clears all the existing markers
			mMap.clear();

			for (int i = 0; i < list.size(); i++) {

				HashMap<String, String> hmPlace = list.get(i);
				String name = hmPlace.get("formatted_address");

				String[] split = name.split(",");

				// TODO: pasar esto a propertie e ignorar los acentos.
				if (split[1].contains("C�rdoba")
						|| split[1].contains("Cordoba")) {

					hideKeyboard();

					// Creating a marker
					// MarkerOptions markerOptions = new MarkerOptions();

					double lat = Double.parseDouble(hmPlace.get("lat"));
					double lng = Double.parseDouble(hmPlace.get("lng"));

					LatLng latLng = new LatLng(lat, lng);
					// marker.setPosition(latLng);
					// markerOptions.position(latLng);
					// markerOptions.title(name);

					// mMap.addMarker(markerOptions);

					// Locate the first location
					// if (i == 0) {
					mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
					locationStreet = name;
					locationLatLng = latLng;
					// setUpMap();

					MarkerOptions a = new MarkerOptions();
					a.position(latLng);
					//
					a.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.ic_marker));
					marker = mMap.addMarker(a);

					// }

					break;
				} else {
					Toast.makeText(
							getBaseContext(),
							"No se encontr� direcci�n. Por favor verificar se ingreso correctamente",
							Toast.LENGTH_SHORT).show();
				}
			}
		}

		private void hideKeyboard() {
			// Lineas para ocultar el teclado virtual (Hide keyboard)
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(etPlace.getWindowToken(), 0);
		}
	}

}
