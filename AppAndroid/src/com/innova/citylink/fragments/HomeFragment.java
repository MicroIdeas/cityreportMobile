package com.innova.citylink.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.innova.citylink.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link HomeFragment#newInstance} factory method
 * to create an instance of this fragment.
 * 
 */
public class HomeFragment extends Fragment {
	
	private TextView tvDescriptionApp = null;

	public static HomeFragment newInstance() {
		HomeFragment fragment = new HomeFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home, container,
				false);
		
		tvDescriptionApp = (TextView)rootView.findViewById(R.id.tv_description_app);
		tvDescriptionApp.setText(Html.fromHtml(getString(R.string.txt_home_description)));

		return rootView;
	}
}
